const multer = require('multer');
const ApiError = require('../class/ApiError');

const upload = multer({
	dest: `${process.env.STORAGE}/uploads`,
	fileFilter: (req, file, cb) => {
		if(['application/x-bittorrent'].indexOf(file.mimetype) === -1) {
			cb(new ApiError(415, "Mime Type not supported, use application/x-bittorent"));
		}

		cb(null, true)
	},
});

module.exports.torrent = upload.single('torrent');