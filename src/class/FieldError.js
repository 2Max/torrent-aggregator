class FieldError extends Error {
  constructor(message, err) {
    super(message);
    this.name = 'FieldError'
    this.previous = {
      message: err ? err.message : '',
    };
  }

  getPrevious() {
    return this.previous;
  }
}

module.exports = FieldError;