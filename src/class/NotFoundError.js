class NotFoundError extends Error {
  constructor(message, err) {
    super(message);
    this.name = 'NotFoundError'
    this.previous = {
      message: err ? err.message : '',
    };
  }

  getPrevious() {
    return this.previous;
  }
}

module.exports = NotFoundError;