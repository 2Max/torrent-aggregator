class NotConnectedError extends Error {
  constructor(message, err) {
    super(message);
    this.name = 'NotConnectedError'
    this.previous = {
      message: err ? err.message : '',
    };
  }

  getPrevious() {
    return this.previous;
  }
}

module.exports = NotConnectedError;