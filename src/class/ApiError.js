class ApiError extends Error {
	constructor(status, message, err) {
		super(message);
		this.status = status;
		this.previous = {
			message: err ? err.message : '',
		};
	}

	getStatus() {
		return this.status;
	}

	getPrevious() {
		return this.previous;
	}

	body() {
		return {
			message: this.message,
			previous: this.getPrevious(),
		}
	}
}

module.exports = ApiError;