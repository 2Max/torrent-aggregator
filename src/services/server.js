require('dotenv').config();
const fs = require('fs');
const logger = require('./logger');
const wTorrent = require('wtorrent');
const NotFoundError = require('../class/NotFoundError');
const NotConnectedError = require('../class/NotConnectedError');
const DuplicateError = require('../class/DuplicateError');
const FieldError = require('../class/FieldError');
const DATA_DIR = process.env.STORAGE;
const DATA_FILE = {
	SERVERS: 'servers.json',
};

/**
 * @param data
 * @param original
 * @returns {{connected: (boolean|{dataTransform: (function(*): (*))}), endpoint: (null|string), password: (null|string), port: (*|number), name: (*|null), host: (*|null), client: null, user: (null|number|PublicKeyCredentialUserEntity)}}
 */
function createServerModel(data, original) {
	return {
		client: data.client ? data.client : (original ? original.client : null),
		name: data.name ? data.name : (original ? original.name : null),
		host: data.host ? data.host : (original ? original.host : null),
		port: data.port ? data.port : (original ? original.port : -1),
		endpoint: data.endpoint ? data.endpoint : (original ? original.endpoint : null),
		user: data.user ? data.user : (original ? original.user : null),
		password: data.password ? data.password : (original ? original.password : null),
		connected: data.connected ? data.connected : (original ? original.connected : false),
	}
}

/**
 * Get servers
 * @returns {*}
 */
function getServers(shouldConnected) {
	const data = fs.readFileSync(`${DATA_DIR}/${DATA_FILE.SERVERS}`);
	const servers = JSON.parse(data);
	const _servers = [];

	for(let i=0; i<servers.length; i++) {
		if(!shouldConnected || (shouldConnected && servers[i].connected)) {
			_servers.push(servers[i]);
		}
	}

	return _servers;
}

/**
 * Get server by name
 * @param name
 * @param shouldConnected
 * @returns {*}
 */
function getServer(name, shouldConnected) {
	const data = fs.readFileSync(`${DATA_DIR}/${DATA_FILE.SERVERS}`);
	const servers = JSON.parse(data);
	for(let i=0; i<servers.length; i++) {
		if(name === servers[i].name) {
			if(shouldConnected && !servers[i].connected) {
				throw new NotConnectedError('This server is not connected');
			}

			return servers[i];
		}
	}

	throw new NotFoundError(`This server does not exists : ${name}`);
}

/**
 * Add torrent server
 * @param options
 * @param noCheck
 */
async function addServer(options, noCheck=true) {
	createDefaultDataFile(`${DATA_DIR}/${DATA_FILE.SERVERS}`, []);
	const data = fs.readFileSync(`${DATA_DIR}/${DATA_FILE.SERVERS}`);
	const servers = JSON.parse(data);

	let serverAlreadyExists;
	try {
		serverAlreadyExists = getServer(options.name);
		if(serverAlreadyExists) {
			throw new DuplicateError('This name already exists');
		}
	} catch(e) {
		if(e.name !== 'NotFoundError') {
			throw e;
		}
	}

	if((serverAlreadyExists = isServerExists(servers, options))) {
		throw new DuplicateError(`Server already registered : ${serverAlreadyExists.name}`);
	}

	options = checkServerFields(options);

	if(!noCheck) {
		try {
			await wTorrent(options);
		} catch(e) {
			throw new NotConnectedError(`Server cannot be connect: ${options.name}`, e);
		}
	}

	const server = createServerModel({
		...options,
		connected: !noCheck,
	}, null);

	servers.push(server);
	fs.writeFileSync(`${DATA_DIR}/${DATA_FILE.SERVERS}`, JSON.stringify(servers), {encoding: 'utf8'});
	return server;
}

/**
 * @param options
 * @returns {Promise<Client>|Wrapper}
 */
async function connect(options) {
	try {
		const client = await wTorrent(options);
		updateServerWithKeyValue(options.name, 'connected', true);
		return client;
	} catch(e) {
		updateServerWithKeyValue(options.name, 'connected', false);
		throw new NotConnectedError(`Server cannot be connected: ${options.name}`, e);
	}
}

/**
 * Update server by name
 * @param name
 * @param server
 */
function updateServer(name, server) {
	const data = fs.readFileSync(`${DATA_DIR}/${DATA_FILE.SERVERS}`);
	const servers = JSON.parse(data);
	let newServer = null;
	for(let i=0; i<servers.length; i++) {
		if(name === servers[i].name) {
			servers[i] = createServerModel(server, servers[i]);
			newServer = servers[i];
		}
	}

	fs.writeFileSync(`${DATA_DIR}/${DATA_FILE.SERVERS}`, JSON.stringify(servers), {encoding: 'utf8'});
	return newServer;
}

/**
 * Remove torrent server
 * @param name
 * @returns {boolean}
 */
function removeServer(name) {
	const data = fs.readFileSync(`${DATA_DIR}/${DATA_FILE.SERVERS}`);
	const servers = JSON.parse(data);
	const index = servers.map((s) => s.name).indexOf(name);

	if(index === -1) {
		throw new NotFoundError(`This server does not exists : ${name}`);
	}

	servers.splice(index, 1);
	fs.writeFileSync(`${DATA_DIR}/${DATA_FILE.SERVERS}`, JSON.stringify(servers), {encoding: 'utf8'});
	return true;
}

/**
 * Update server by name
 * @param name
 * @param key
 * @param value
 */
function updateServerWithKeyValue(name, key, value) {
	const data = fs.readFileSync(`${DATA_DIR}/${DATA_FILE.SERVERS}`);
	const servers = JSON.parse(data);
	let newServer = null;
	for(let i=0; i<servers.length; i++) {
		if(name === servers[i].name) {
			servers[i][key] = value;
			newServer = servers[i];
		}
	}

	fs.writeFileSync(`${DATA_DIR}/${DATA_FILE.SERVERS}`, JSON.stringify(servers), {encoding: 'utf8'});
	return newServer;
}



/**
 * List of all torrents
 * @param name
 * @returns {Promise<Array>}
 */
async function list(name) {
	let torrents = [];
	const data = fs.readFileSync(`${DATA_DIR}/${DATA_FILE.SERVERS}`);
	const servers = JSON.parse(data);

	for(let i=0; i<servers.length; i++) {
		if(!servers[i].connected) {
			continue;
		}

		if(name && name !== servers[i].name) {
			continue;
		}

		try {
			const client = await connect(servers[i]);
			torrents = torrents.concat((await client.get(true))
				.map((torrent) => ({
					...torrent,
					server: servers[i].name,
					client: servers[i].client,
					configServer: servers[i],
				})));
		} catch(e) {
			updateServerWithKeyValue(servers[i].name, 'connected', false);
			logger.write(`Unable to connect to torrent server : ${servers[i].name}`, logger.LEVEL.ERROR);
		}
	}

	return torrents;
}

function createDefaultDataFile(path, value) {
	try {
		fs.accessSync(path, fs.constants.F_OK | fs.constants.W_OK | fs.constants.R_OK);
	} catch(e) {
		fs.writeFileSync(path, JSON.stringify(value), {
			encoding: 'utf8',
			flag: 'wx'
		});
	}
}

/**
 * @param servers
 * @param host
 * @param port
 * @returns {*}
 */
function isServerExists(servers, {host, port}) {
	for(let i=0; i<servers.length;i++) {
		if(servers[i].host === host && servers[i].port === port) {
			return servers[i];
		}
	}

	return false;
}

/**
 * Check servr's fields type
 * @param fields
 */
function checkServerFields(fields) {
	const requiredFields = ['host', 'port', 'endpoint', 'client'];
	for(let i=0; i<requiredFields.length; i++) {
		if(fields[requiredFields[i]] === null || fields[requiredFields[i]] === undefined || fields[requiredFields[i]] === '') {
			throw new FieldError(`${requiredFields[i]} is required`);
		}
	}

	const numberFields = ['port'];
	for(let i=0; i<numberFields.length; i++) {
		if(fields[numberFields[i]] && isNaN(fields[numberFields[i]])) {
			throw new FieldError(`${numberFields[i]} must be a number`);
		} else {
			fields[numberFields[i]] = Number(fields[numberFields[i]]);
		}
	}

	const stringFields = ['name', 'host','endpoint','user','password'];
	for(let i=0; i<stringFields.length; i++) {
		if(fields[stringFields[i]] && typeof fields[stringFields[i]] !== 'string') {
			throw new FieldError(`${stringFields[i]} must be a string`);
		}
	}

	const availableClients = ['wtorrent-rtorrent', 'wtorrent-transmission'];
	if(availableClients.indexOf(fields.client) === -1) {
		throw new FieldError('Client is not supported');
	}

	return fields;
}

module.exports = {
	connect,
	addServer,
	removeServer,
	getServer,
	getServers,
	updateServer,
	updateServerWithKeyValue,
	list,
	checkServerFields,
};
