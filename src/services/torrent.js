const serverService = require('./server');
const NotFoundError = require('../class/NotFoundError');

module.exports.getHashesFromServer = async(server) => {
	const client = await serverService.connect(server);
	return client.get(false, false);
}

module.exports.getFromServer = async(server, filters) => {
	const client = await serverService.connect(server);
	let torrents = await client.get(true, !!filters.files)
	for(let i=0; i<torrents.length; i++) {
		if(filters['server']) {
			torrents[i] = {
				...torrents[i],
				server: {
					name: server.name,
					client: server.client,
				}
			}
		}
	}

	if(filters['ratio']) {
		torrents = torrents.filter(torrent => torrent.ratio >= parseInt(filters.ratio));
	}

	if(filters['active']) {
		torrents = torrents.filter(torrent => torrent.active);
	}

	if(filters['stopped']) {
		torrents = torrents.filter(torrent => !torrent.active);
	}

	return torrents;
}

module.exports.getOne = async(server, hash, withFiles) => {
	try {
		const client = await serverService.connect(server);
		return (await client.getOne(hash, withFiles))
	} catch(e) {
		if(e.message.match(/Could not find info-hash/) !== null) {
			throw new NotFoundError(`Could not find info-hash ${hash}`);
		} else {
			throw e;
		}
	}
};

module.exports.play = async(server, hash) => {
	try {
		const client = await serverService.connect(server);
		return (await client.play(hash))
	} catch(e) {
		if(e.message.match(/Could not find info-hash/) !== null) {
			throw new NotFoundError(`Could not find info-hash ${hash}`);
		} else {
			throw e;
		}
	}
};

module.exports.pause = async(server, hash) => {
	try {
		const client = await serverService.connect(server);
		return (await client.pause(hash));
	} catch(e) {
		if(e.message.match(/Could not find info-hash/) !== null) {
			throw new NotFoundError(`Could not find info-hash ${hash}`);
		} else {
			throw e;
		}
	}
};

module.exports.remove = async(server, hash) => {
	try {
		const client = await serverService.connect(server);
		return (await client.remove(hash));
	} catch(e) {
		if(e.message.match(/Could not find info-hash/) !== null) {
			throw new NotFoundError(`Could not find info-hash ${hash}`);
		} else {
			throw e;
		}
	}
};

module.exports.getFiles = async(server, hash) => {
	try {
		const client = await serverService.connect(server);
		return (await client.getFiles(hash));
	} catch(e) {
		if(e.message.match(/Could not find info-hash/) !== null) {
			throw new NotFoundError(`Could not find info-hash ${hash}`);
		} else {
			throw e;
		}
	}
};

module.exports.createFromBuffer = async(server, buffer) => {
	const client = await serverService.connect(server);
	return client.createFromBuffer(buffer);
};

module.exports.createFromFile = async(server, file) => {
	const client = await serverService.connect(server);
	return client.createFromFile(file);
};
