# Torrent Aggregator

Aggregate torrent's servers in one place

Clients available :
- rTorrent (wtorrent-rtorrent)
- Transmission (wtorrent-transmission)

## Installation

- Pre config

```bash
echo STORAGE={STORAGE} > .env
mkdir -p {STORAGE}/uploads
sudo chown -R 1000:1000 {STORAGE}
nvm install
npm install
```

## Usage

####Command line

```bash
node index server:add Toto -c wtorrent-rtorrent -h 127.0.0.1 -p 8080 -e /RPC2 -u admin -P nimda

node index torrent:all
Toto rtorrent E68BC739C8D361CD238880389E910CC9566F0FAF Tout.Nous.Separe.2017.VOF.1080p.mHD.x264.AC3.5.1-SEL                            3 Go   3 Go       3 Go     1.133 true  
Toto rtorrent A98E5C8BC8E6072BAC2867098417D731AD7290F9 Dial.M.For.Murder.1954.1080p.BluRay.x265.DTS-AZAZE.mkv                          4 Go   4 Go       1 Go     0.124 true 
...

node index torrent:add Toto < my_file.torrent
```

#### Docker

```bash
docker-compose up -d
```

```bash
docker-compose run --rm aggregator torrent:all
Toto rtorrent E68BC739C8D361CD238880389E910CC9566F0FAF Tout.Nous.Separe.2017.VOF.1080p.mHD.x264.AC3.5.1-SEL                            3 Go   3 Go       3 Go     1.133 true  
Toto rtorrent A98E5C8BC8E6072BAC2867098417D731AD7290F9 Dial.M.For.Murder.1954.1080p.BluRay.x265.DTS-AZAZE.mkv                          4 Go   4 Go       1 Go     0.124 true 
...
```

#### API
```bash
node index server:listen -p 8092 # default 8080
```


## Documentation

### Command line

##### Add server torrent

For add rTorrent server :

```bash
node index server:add Toto -c wtorrent-rtorrent -h 127.0.0.1 -p 80 -e /RPC2
```

for add transmission server :

```bash
node index server:add Toto -c wtorrent-transmission -h 127.0.0.1 -p 9091 -e /transmission/rpc
```

##### Connect to server

```bash
node index server:connect Toto -u admin -P nimda
```

##### Disconnect to server

```bash
node index server:disconnect Toto
```

##### Get all servers

```bash
node index server:all
```

##### Get one server

```bash
node index server:one Toto
```

##### Update server

```bash
node index server:set Toto <field> <value>
```

##### Remove server

```bash
node index server:rm Toto
```

##### List all torrents

```bash
node index torrent:all
```

*Only specific server*
```bash
node index torrent:get Toto
```

*Only ratio equal or superior of 1*

```bash
node index torrent:all --ratio=1
```

*Only active torrents*

```bash
node index torrent:all --active
```

*Only stopped torrents*

```bash
node index torrent:all --stopped
```

##### Get one torrent

```bash
node index torrent:one Toto 51059042B70107CB147C31
```

##### Play one torrent

```bash
node index torrent:play Toto 51059042B70107CB147C31
```

##### Pause one torrent

```bash
node index torrent:pause Toto 51059042B70107CB147C31
```

##### Remove one torrent

*This command remove only .torrent file, no data*

```bash
node index torrent:rm 51059042B70107CB147C31
```

##### Get files of torrent

```bash
node index torrent:files Toto 51059042B70107CB147C31
```

##### Add torrent from file

```bash
node index torrent:add Toto -f /path/to/file
node index torrent:add Toto < /path/to/file
```

### Http API

**You should add JWT_SECRET**

```markdown
GET: /servers
POST: /servers
GET: /servers/:name	
PATCH: /servers/:name	
DELETE: /servers/:name
POST: /servers/:name/upload
GET: /servers/test

GET: /torrents
GET: /servers/:name/torrents
GET: /servers/:name/torrents/:hash?files=1
PATCH: /servers/:name/torrents/:hash?action=pause|resume
DELETE: /servers/:name/torrents/:hash
GET: /servers/:name/torrents/:hash/files
```