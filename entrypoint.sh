#!/bin/sh

if [ "$1" = "server:listen" ]
then
  chown -R node:node /var/data
  su -p - node
  if [ "$NODE_ENV" = "development" ]
  then
    su node -c "/usr/local/bin/node ./node_modules/.bin/nodemon index.js $@"
    exit
  else
    su node -c "/usr/local/bin/node index.js $@"
    exit
  fi
fi

/usr/local/bin/node index.js "$@"